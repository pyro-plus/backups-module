<?php namespace Defr\BackupsModule\Dump\Command;

use Anomaly\Streams\Platform\Application\Application;
use Anomaly\Streams\Platform\Message\MessageBag;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\DB;

/**
 * Class for delete dump from the filesystem
 *
 * @package defr.module.backups
 *
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 */
class RestoreBackup
{

    /**
     * The path of file
     *
     * @var mixed
     */
    protected $path;

    /**
     * Create an instance of RestoreBackup class
     *
     * @param mixed $path The path
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * Handle the command
     *
     * @param  Filesystem $files The files
     * @return string
     */
    public function handle(Filesystem $files, MessageBag $messages)
    {
        if (!$files->exists($this->path))
        {
            return 'Dump file not found!';
        }

        if (!$data = json_decode($files->get($this->path), true))
        {
            return 'JSON syntax error!';
        }

        $appReference = app(Application::class)->getReference();

        foreach ($data as $table => $rows)
        {
            $table = preg_replace('/^'. $appReference . '_/', '', $table);

            if (starts_with($table, 'applications'))
            {
                continue;
            }

            DB::table($table)->truncate();

            foreach ($rows as $row)
            {
                DB::table($table)->insert($row);
            }

            $count = count($rows);

            echo "\033[37;5;228mTable \033[31;5;228m{$appReference}_{$table} ";
            echo "\033[37;5;228mrestored with \033[31;5;228m{$count} \033[37;5;228mrows.\n";
        }

        return count($data);
    }
}
