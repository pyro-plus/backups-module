<?php namespace Defr\BackupsModule\Dump\Console;

use Defr\BackupsModule\Dump\Command\CreateBackup;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Symfony\Component\Console\Input\InputOption;

/**
 * Artisan make backup command class
 *
 * @package defr.module.backups
 *
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 */
class MakeCommand extends Command
{
    use DispatchesJobs;

    /**
     * Command name
     *
     * @var string
     */
    protected $name = 'backup:make';

    /**
     * Command description
     *
     * @var string
     */
    protected $description = 'Make the backup';

    /**
     * Run the command
     *
     * @throws \Exception
     */
    public function fire()
    {
        $start = microtime();

        if (!$path = $this->dispatch(new CreateBackup(
            $this->option('connection'),
            $this->option('tables'),
            $this->argument('addon')
        )))
        {
            throw new \Exception('Error! Can\'t create backup!');
        }

        $size = str_replace('&nbsp;', ' ', filesize_for_humans(filesize($path)));
        $time = microtime() - $start;

        $this->info('Dump created successfully!');
        $this->warn("File path: `{$path}`");
        $this->info("Size: {$size}. Time: {$time} sec.");
    }

    /**
     * Gets the arguments of a command
     *
     * @return array The arguments.
     */
    protected function getArguments()
    {
        return [
            ['addon', null, InputOption::VALUE_OPTIONAL, 'Addon, in dot notation.'],
        ];
    }

    /**
     * Gets the options of a command
     *
     * @return array The options.
     */
    protected function getOptions()
    {
        return [
            ['connection', null, InputOption::VALUE_OPTIONAL, 'DB connection to use.'],
            ['tables', null, InputOption::VALUE_OPTIONAL, 'Tables to include in the dump.'],
        ];
    }
}
