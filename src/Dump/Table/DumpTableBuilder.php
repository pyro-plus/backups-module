<?php namespace Defr\BackupsModule\Dump\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

/**
 * Class for building a table
 *
 * @package defr.module.backups
 *
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 */
class DumpTableBuilder extends TableBuilder
{

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'title'              => [
            'wrapper' => '<h5>{value}</h5>',
        ],
        'path'               => [
            'heading' => 'module::table.filename.name',
            'value'   => 'entry.file_name',
            'wrapper' => '<strong>{value}</strong>',
        ],
        'addon'              => [
            'value'   => 'entry.addon_name',
            'wrapper' => '<strong>{value}</strong>',
        ],
        'entry.size'         => [
            'wrapper' => '<h6>{value}</h6>',
        ],
        'entry.tables_count' => [
            'heading' => 'module::table.count.name',
            'wrapper' => '<center>{value}</center>',
        ],
        'created_at'         => [
            'value' => 'entry.created_at_datetime',
        ],
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'restore'     => [
            'href' => 'admin/backups/restore/{entry.id}',
        ],
        'information' => [
            'data-toggle' => 'modal',
            'data-target' => '#modal-wide',
            'href'        => 'admin/backups/info/{entry.id}',
        ],
        'edit',
        'delete'      => [
            'href' => 'admin/backups/delete/{entry.id}',
        ],
    ];
}
