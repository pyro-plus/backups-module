<?php namespace Defr\BackupsModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

/**
 * Module class
 *
 * @package defr.module.backups
 *
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 */
class BackupsModule extends Module
{

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-database';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'dumps' => [
            'buttons' => [
                'new_dump',
                'change' => [
                    'text'        => 'defr.module.backups::button.change',
                    'href'        => 'admin/backups/choose',
                    'data-toggle' => 'modal',
                    'data-target' => '#modal',
                ],
            ],
        ],
    ];
}
