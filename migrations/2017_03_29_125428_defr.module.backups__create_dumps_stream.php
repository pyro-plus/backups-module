<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleBackupsCreateDumpsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'dumps',
        'title_column' => 'title',
        'order_by'     => 'created_at',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'title',
        'db_connection' => [
            'required' => true,
        ],
        'addon',
        'path',
    ];

}
