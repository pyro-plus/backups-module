<?php namespace Defr\BackupsModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Anomaly\Streams\Platform\Model\Backups\BackupsDumpsEntryModel;
use Defr\BackupsModule\Dump\Console\ListCommand;
use Defr\BackupsModule\Dump\Console\MakeCommand;
use Defr\BackupsModule\Dump\Console\RestoreCommand;
use Defr\BackupsModule\Dump\Contract\DumpRepositoryInterface;
use Defr\BackupsModule\Dump\DumpModel;
use Defr\BackupsModule\Dump\DumpRepository;

// use Defr\BackupsModule\Command\RegisterButtons;

/**
 * Module service provider class
 *
 * @package defr.module.backups
 *
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 */
class BackupsModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Module bindings
     *
     * @var array
     */
    protected $bindings = [
        BackupsDumpsEntryModel::class => DumpModel::class,
    ];

    /**
     * Module singletons
     *
     * @var array
     */
    protected $singletons = [
        DumpRepositoryInterface::class => DumpRepository::class,
    ];

    /**
     * Module commands
     *
     * @var array
     */
    protected $commands = [
        MakeCommand::class,
        ListCommand::class,
        RestoreCommand::class,
    ];

    /**
     * Module routes
     *
     * @var array
     */
    protected $routes = [
        'admin/backups'              => 'Defr\BackupsModule\Http\Controller\Admin\DumpsController@index',
        'admin/backups/create'       => 'Defr\BackupsModule\Http\Controller\Admin\DumpsController@create',
        'admin/backups/choose'       => 'Defr\BackupsModule\Http\Controller\Admin\DumpsController@choose',
        'admin/backups/edit/{id}'    => 'Defr\BackupsModule\Http\Controller\Admin\DumpsController@edit',
        'admin/backups/info/{id}'    => 'Defr\BackupsModule\Http\Controller\Admin\DumpsController@info',
        'admin/backups/delete/{id}'  => 'Defr\BackupsModule\Http\Controller\Admin\DumpsController@delete',
        'admin/backups/restore/{id}' => 'Defr\BackupsModule\Http\Controller\Admin\DumpsController@restore',
    ];
}
