<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleBackupsCreateBackupsFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'title'         => 'anomaly.field_type.text',
        'path'          => 'anomaly.field_type.text',
        'db_connection' => 'anomaly.field_type.select',
        'addon'         => 'anomaly.field_type.checkboxes',
    ];

}
