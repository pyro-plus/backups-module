<?php namespace Defr\BackupsModule\Dump\Console;

use Defr\BackupsModule\Dump\Command\GetBackups;
use Defr\BackupsModule\Dump\Command\RestoreBackup;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Artisan backups list command class
 *
 * @package    defr.module.backups
 *
 * @author     Denis Efremov <efremov.a.denis@gmail.com>
 */
class RestoreCommand extends Command
{
    use DispatchesJobs;

    /**
     * Command signature
     *
     * @var string
     */
    protected $signature = 'backup:restore';

    /**
     * Command name
     *
     * @var string
     */
    protected $name = 'Backup restore';

    /**
     * Command description
     *
     * @var string
     */
    protected $description = 'Restoring choosen backup';

    /**
     * Run the command
     */
    public function fire()
    {
        /* @var DumpCollection $backups */
        $backups = $this->dispatch(new GetBackups());

        if (!$backups->count())
        {
            throw new \Exception('Error! There is no one backup found!');
        }

        $info = $this->choice(
            'You should select the backup file',
            $backups->map(
                function ($backup)
                {
                    return $backup->path . ' ' . $backup->getPresenter()->getConsoleSize();
                }
            )->all()
        );

        $path = array_get(explode(' ', $info), 0);

        $message = $this->dispatch(new RestoreBackup($path));

        if (!is_integer($message) && is_string($message))
        {
            throw new \Exception($message);
        }

        return $this->info($message . ' tables restored successfully.');
    }
}
