<?php namespace Defr\BackupsModule\Dump\Command;

use Anomaly\SettingsModule\Setting\Contract\SettingRepositoryInterface;
use Defr\BackupsModule\Dump\Contract\DumpRepositoryInterface;
use Illuminate\Filesystem\Filesystem;

/**
 * Class for get dumps from the filesystem
 *
 * @package    defr.module.backups
 *
 * @author     Denis Efremov <efremov.a.denis@gmail.com>
 */
class GetBackups
{

    /**
     * Handle the command
     *
     * @param  SettingRepositoryInterface $settings The settings
     * @param  DumpRepositoryInterface    $dumps    The dumps
     * @param  Filesystem                 $files    The files
     * @return DumpCollection
     */
    public function handle(
        SettingRepositoryInterface $settings,
        DumpRepositoryInterface $dumps,
        Filesystem $files
    )
    {
        $path = base_path(env(
            'DUMPS_PATH',
            $settings->value('defr.module.backups::dump_path', 'dumps')
        ));

        $list = $files->glob($path . '/*sql.json');

        return $dumps->sync(array_combine(
            $list,
            array_map(
                function ($path) use ($files)
                {
                    return [
                        'path'    => $path,
                        'content' => json_decode($files->get($path), true),
                    ];
                },
                $list
            )
        ));
    }
}
