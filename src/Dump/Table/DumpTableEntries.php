<?php namespace Defr\BackupsModule\Dump\Table;

use Defr\BackupsModule\Dump\Command\GetBackups;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class for overriding table entries
 *
 * @package defr.module.backups
 *
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 */
class DumpTableEntries
{
    use DispatchesJobs;

    /**
     * Handle the command
     *
     * @param DumpFormBuilder $builder The builder
     */
    public function handle(DumpTableBuilder $builder)
    {
        $builder->setTableEntries($this->dispatch(new GetBackups()));
    }
}
