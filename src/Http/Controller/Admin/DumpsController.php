<?php namespace Defr\BackupsModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Defr\BackupsModule\Dump\Command\DeleteBackup;
use Defr\BackupsModule\Dump\Command\LoadInfo;
use Defr\BackupsModule\Dump\Command\RestoreBackup;
use Defr\BackupsModule\Dump\Contract\DumpRepositoryInterface;
use Defr\BackupsModule\Dump\Form\DumpFormBuilder;
use Defr\BackupsModule\Dump\Table\DumpTableBuilder;
use Illuminate\Contracts\Config\Repository;
use Anomaly\Streams\Platform\Message\MessageBag;

/**
 * Dumps admin controller
 *
 * @package defr.module.backups
 *
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 */
class DumpsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param  DumpTableBuilder $table
     * @return Response
     */
    public function index(DumpTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param  DumpFormBuilder $form
     * @return Response
     */
    public function create(DumpFormBuilder $form)
    {
        $form->setDbConnection($this->request->get('db_connection'));

        return $form->render();
    }

    /**
     * Create a new entry.
     *
     * @param  DumpFormBuilder $form
     * @param  Repository      $config The configuration
     * @return Response
     */
    public function choose(DumpFormBuilder $form, Repository $config)
    {
        $connections = $config->get('database.connections');

        return view(
            'defr.module.backups::admin.dumps.choose_connection',
            compact('connections')
        );
    }

    /**
     * Edit an existing entry.
     *
     * @param  DumpRepositoryInterface $dumps  The dumps
     * @param  DumpFormBuilder         $form
     * @param  int                     $id
     * @return Response
     */
    public function edit(DumpRepositoryInterface $dumps, DumpFormBuilder $form, $id)
    {
        $entry = $dumps->find($id);

        $form->setDbConnection($entry->getDbConnection());

        return $form->render($id);
    }

    /**
     * Information about an existing entry.
     *
     * @param  DumpFormBuilder $form
     * @param  $id
     * @return Response
     */
    public function info($id)
    {
        return $this->dispatch(new LoadInfo($id));
    }

    /**
     * Delete an entry
     *
     * @param  DumpTableBuilder $table The table
     * @param  mixed            $id    The identifier
     * @return Response
     */
    public function delete(MessageBag $messages, $id)
    {
        $message = $this->dispatch(new DeleteBackup($id));

        if ($message === true)
        {
            return $this->redirect->back()->withInput()
                ->withErrors($messages->success('Backup deleted successfully.'));
        }

        return $this->redirect->back()->withInput()
            ->withErrors($messages->error($message));
    }

    /**
     * Restore a dump
     *
     * @param  DumpTableBuilder $table The table
     * @param  int              $id    The identifier
     * @return Response
     */
    public function restore(MessageBag $messages, $id)
    {
        $message = $this->dispatch(new RestoreBackup($id));

        if (is_integer($message))
        {
            return $this->redirect->back()->withInput()
                ->withErrors($messages->success($message . ' tables restored successfully.'));
        }

        return $this->redirect->back()->withInput()
            ->withErrors($messages->error($message));
    }
}
