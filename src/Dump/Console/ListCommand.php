<?php namespace Defr\BackupsModule\Dump\Console;

use Defr\BackupsModule\Dump\Command\GetBackups;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Artisan backups list command class
 *
 * @package    defr.module.backups
 *
 * @author     Denis Efremov <efremov.a.denis@gmail.com>
 */
class ListCommand extends Command
{
    use DispatchesJobs;

    /**
     * Command signature
     *
     * @var string
     */
    protected $signature = 'backup:list';

    /**
     * Command name
     *
     * @var string
     */
    protected $name = 'Backups list';

    /**
     * Command description
     *
     * @var string
     */
    protected $description = 'Show the list of created backups';

    /**
     * Run the command
     */
    public function fire()
    {
        $backups = $this->dispatch(new GetBackups());

        $this->table(
            [
                'ID',
                'Created at',
                'Path to the backup file',
                'Backup size'
            ],
            $backups->map(function ($backup)
            {
                return [
                    $backup->getId(),
                    $backup->getCreatedAt(),
                    $backup->getPath(),
                    $backup->getPresenter()->getConsoleSize(),
                ];
            })
        );
    }
}
