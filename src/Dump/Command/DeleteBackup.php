<?php namespace Defr\BackupsModule\Dump\Command;

use Illuminate\Filesystem\Filesystem;

/**
 * Class for delete backup from the filesystem
 *
 * @package defr.module.backups
 *
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 */
class DeleteBackup
{

    /**
     * The path of file
     *
     * @var mixed
     */
    protected $path;

    /**
     * Create an instance of DeleteBackup class
     *
     * @param mixed $path The path
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * Handle the command
     *
     * @param  Filesystem $files The files
     * @return string
     */
    public function handle(Filesystem $files)
    {
        if (!$files->exists($this->path))
        {
            return 'Backup file not found!';
        }

        if (!$files->delete([$this->path]))
        {
            return 'Can\'t remove backup!';
        }

        return true;
    }
}
